import moment from "moment";

export const DatePickertoString = (value: any) => {
    if (!value) {
        return "";
    }
    return value.slice(0, 10).replace(/[-:]/g, '');
}

export const DateFormatDatePicker = (value: any) => {
    if (!value) {
        return "";
    }
    return moment(value).format("DD MMM YYYY")
}

export const DateRangePickertoString = (value: any) => {
    if (!value) {
        return "";
    }
    return  moment(value).format('YYYYMMDD');
}

export const StringToDate = (value: any) => {
    if (!value) {
        return "";
    }
    return moment(value, 'YYYYMMDD').format("DD MMM YYYY")
}

export const DateFormat = (value: any) => {
    if (!value) {
        return "";
    }
    var date = moment(value, 'YYYYMMDDHHmmss')
    return date.format("DD MMM YYYY")
}

export const DateFormatYm = (value: any) => {
    if (!value) {
        return "";
    }
    var date = moment(value, 'YYYYMM')
    return date.format("YYYY-MM")
}

export const DateFormatTime = (value: any) => {
    if (!value) {
        return "";
    }
    var date = moment(value, 'YYYYMMDDHHmmss')
    return date.format("DD MMM YYYY HH:mm:ss A")
}

export const DateTimeFormatBillActivity = (value: any) => {
    if (!value) {
        return "";
    }
    var date = moment(value, 'YYYYMMDDHHmmss')
    return date.format("DD MMM YYYY @ hh:mm A")
}

export const getDate8 = (value: any) => {
    if (!value) {
        return "";
    }
    return moment(value).format("YYYYMMDD")
}

export const dateNow = () => {
    return moment().format("YYYY-MM-DD")
}

export const dateTimeNow = () => moment().format("YYYYMMDDHHmmss")

export const timeNow = () => moment().format("HH:mm")

export const timeNowhhmm = () => moment().format("hh:mm")

export const getDueDateBaseOnPaymentTerm = (value: any, paymentTerm: any) => {

    if (!value) {
        return "";
    }
    return moment(value).add(paymentTerm, 'days').format('YYYY-MM-DD')
}

export const getDayFromDate = (value: any) => {
    if (!value) {
        return "";
    }
    return moment(value, "YYYYMMDDHHmmss").format("DD")
}

export const getDayOfWeekFromDate = (value: any) => {
    if (!value) {
        return "";
    }
    return moment(value, "YYYYMMDDHHmmss").days() + ''
}

export const getIssueDateAddDays = (value: string) => {
    let now =  moment().format('YYYY-MM-DD');
    let givenDate = moment().days(value);
    let req = moment();

    if(givenDate.format('YYYY-MM-DD') < now){
        req = givenDate.add(1, 'weeks');
    }else{
        req = req.days(value)
    }

    return req.format('YYYY-MM-DD');
}

export const getIssueDateAddMonths = (value: string) => {
    let now =  moment().format('YYYY-MM-DD');
    let firstDayOfMonth = moment().startOf("month");

    let givenDate = firstDayOfMonth.add(Number(value) - 1 , 'days');

    let req;

    if(givenDate.format('YYYY-MM-DD') < now ){
        req = givenDate.add(1, 'months');
    }else{
        req = givenDate;
    }

    return req.format('YYYY-MM-DD');
}
export const DateFormatYmonth = (dateStr: string) => {
    // Extract the date from the string 
    const dateMatch = dateStr.match(/(\d{2})-(\d{2})-(\d{4})/);
    if (!dateMatch) {
        return dateStr;
    }

    // Rearrange the date string to a supported format
    const rearrangedDateStr = `${dateMatch[3]}-${dateMatch[2]}-${dateMatch[1]}`;

    // Create a new Date object
    const date = new Date(rearrangedDateStr);

    // Format the date
    const month = date.toLocaleString('en-US', { month: 'long' });
    const year = date.toLocaleString('en-US', { year: 'numeric' });

    const formattedDate = `${month}, ${year}`;
    return formattedDate;
}


export const getDueDatetime = (value: any) => {
    if (!value) {
        return "";
    }
    return moment(value).format("YYYYMMDD") + moment(value).subtract('1', 'seconds').format("HHmmss")
}

export const DateTimeFormat = (value: any) => {
    if (!value) {
        return "";
    }
    var date = moment(value, 'YYYYMMDDHHmmss')
    return date.format("DD/MMM/YYYY")
}

export const parseToDate = (value: any, pattern: string = 'YYYY-MM-DD') => {
    if (!value) {
        return "";
    }
    return moment(value, 'YYYYMMDDHHmmss').format(pattern)
}

export const getHour = (value: any) => {
    if (!value) {
        return "";
    }
    return moment(value, 'YYYYMMDDHHmmss').format('hh')
}

export const getMinute = (value: any) => {
    if (!value) {
        return "";
    }
    return moment(value, 'YYYYMMDDHHmmss').format('mm')
}

export const getFormatTime = (value: any) => {
    if (!value) {
        return "";
    }
    return moment(value, 'YYYYMMDDHHmmss').format('A')
}

export const parseToTime = (value: any, pattern: string = 'HH:mm') => {
    if (!value) {
        return "";
    }
    return moment(value, 'YYYYMMDDHHmmss').format(pattern)
}

export const TimeFormat = (value: any) => {
    if (!value) {
        return "";
    }
    return moment(value, "HHmmss").format('hh:mm A');
}

export const TimeFormatHour = (value: any) => {
    if (!value) {
        return "";
    }
    return moment(value, "HHmmss").format('hh ');
    
}
export const TimeFormatMinute = (value: any) => {
    if (!value) {
        return "";
    }
    return moment(value, "HHmmss").format('mm');
    
}

export const TimeFormatShiftTime = (value: any) => {
    if (!value) {
        return "";
    }
    return moment(value, "HHmmss").format('A');
    
}

export const formatTime = (value: string) => {
    if (!value) {
        return "";
    }
    moment.updateLocale("en", {
        relativeTime: { future: "in %s", past: "%s ago", s: "sec", m: "1m", mm: "%dm", 
                        h: "1h", hh: "%dh", d: "1d", dd: "%dd", M: "1M", MM: "%dM", y: "1y", yy: "%dy",  
                    },
                 });
    var date = moment(value, 'YYYYMMDDHHmmss')
    return  moment(moment.utc(date).toDate()).fromNow()
}
export const formatDateExcel = (value: Date) => moment(value).format("YYYYMMDD");


export function getDaysArrayByMonth() {
    const firstDayOfMonth = moment().startOf("month");
    const daysInMonth = firstDayOfMonth.daysInMonth();
    const dates = [];
    for (let i = 1; i <= daysInMonth; i++) {
        const date = moment(firstDayOfMonth).add(i - 1, "days");

        dates.push({
            id: date.format("DD"),
            name: date.format("[Day] D"),
        });
    }

    return dates;
}

export const getDaysOfWeek = () => {
    const days = [];
    for (let i = 0; i < 7; i++) {
        const day = moment().startOf("week").add(i, "days").format("dddd");
        days.push({
            id: i,
            name: day
        });
    }
    return days;
};

export const getMonths = () => {
    const months = [];
    for (let i = 0; i < 12; i++) {
        const month = moment().startOf("year").add(i, "months").format("MMMM");
        months.push({
            id: i,
            name: month
        });
    }
    return months;
}

export const getMonthsDate = () => {
    const months = [];
    for (let i = 0; i < 12; i++) {
        const month = moment().startOf("year").add(i, "months").format("MMM");
        months.push({
            id: i,
            name: month
        });
    }
    return months;
}

export const formatIssueDatetime = (value: any) => {
    if (!value) {
        return "";
    }
    return moment(value).format("YYYYMMDD") + moment().format("HHmmss")
}

export const DateTimeFormatSplitOverPay = (value: any) => {
    if (!value) {
        return "";
    }
    var date = moment(value, 'YYYYMMDDHHmmss')
    return date.format("DD MMMM YYYY @ hh:mm A")
}

export const convertToKhmerMonth = (dateString:string) => {
     // Load Khmer locale

    const date = moment(dateString, 'YYYYMMDD');
    date.locale('km'); // Set locale to Khmer
    const formattedDate = date.format('MMMM'); // Format the date to month in Khmer
    return formattedDate;
};
