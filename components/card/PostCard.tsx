import React from 'react';
import { Card, CardHeader, CardBody, CardFooter, Divider, Image } from "@nextui-org/react";
import { Input } from "@nextui-org/input";
import LikeIcon from "@/components/icons/LikeIcon";
import useFetchPostList from "@/lib/hooks/use-fetch-postList";
import {Chip} from "@nextui-org/chip";
import {DateFormat} from "@/utils/dateformat";
import BookmarkIcon from "@/components/icons/BookmarkIcon";
import PostSettingIcon from "@/components/icons/PostSettingIcon";
import {Dropdown, DropdownTrigger, DropdownMenu, DropdownItem, Button, cn} from "@nextui-org/react";
import EditPostIcon from "@/components/icons/EditPostIcon";
import DeletePostIcon from "@/components/icons/DeletePostIcon";
import CommentIcon from "@/components/icons/CommentIcon";
const PostCard = () => {
    const { postList, isLoading, isError } = useFetchPostList();
    const iconClasses = "text-xl text-default-500 pointer-events-none flex-shrink-0";


    if (isLoading) {
        return <p>Loading...</p>;
    }

    if (isError) {
        return <p>Error fetching profile: {isError}</p>;
    }

    return (
        <>
            {postList?.data?.post_list?.map((item: any, index: number) => (
                <Card key={index} className="max-w-[500px] shadow-none">
                    <CardHeader className="w-full flex gap-3">
                        <div className={"flex justify-between w-full"}>
                            <div className={"flex items-center"}>
                                <Image
                                    alt="User profile"
                                    // src={item?.user?.userProfileImg}
                                    className="object-cover rounded-full"
                                    // src={item?.user?.userProfileImg || "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR2NIXc73ZgxZfbifJP3Bsv35sekQyklo-9JA&s"}
                                    src={"https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR2NIXc73ZgxZfbifJP3Bsv35sekQyklo-9JA&s"}
                                    width={40}
                                />
                                <div className="flex flex-col ml-2">
                                    <p className="text-md">{item?.user?.fullName}</p>
                                    <div className={"flex items-center"}>
                                        <p className="text-sm text-default-500">{DateFormat(item?.create_date)}</p>
                                        {/*<p className="text-sm text-default-500">10 May 2024</p>*/}
                                        <span className={"mx-2"}>{"|"}</span>
                                        <Chip size="sm" color="warning" variant="faded">{item?.service?.name}</Chip>
                                    </div>
                                </div>
                            </div>
                            <Dropdown>
                                <DropdownTrigger>
                                    <Button
                                        variant="bordered"
                                        className={"border-none"}
                                    >
                                        <PostSettingIcon />
                                    </Button>
                                </DropdownTrigger>
                                <DropdownMenu variant="faded" aria-label="Dropdown menu with icons">
                                    <DropdownItem
                                        key="new"
                                        startContent={<EditPostIcon />}
                                    >
                                        Edit
                                    </DropdownItem>
                                    <DropdownItem
                                        key="copy"
                                        startContent={<DeletePostIcon />}
                                    >
                                        Delete
                                    </DropdownItem>
                                </DropdownMenu>
                            </Dropdown>
                        </div>


                    </CardHeader>
                    <CardBody>
                        <h4 className="text-small font-semibold leading-none text-default-600 mb-3">{item?.title}</h4>
                        <Image
                            alt="Post image"
                            className="object-cover rounded-sm"
                            src={item?.post_image}
                            width={"100%"}
                        />
                    </CardBody>
                    <CardFooter>
                        <div className={"w-full flex flex-col"}>
                            <div className={"flex justify-between items-center"}>
                                <div className={"flex items-center cursor-pointer"}>
                                    <div className={"flex items-center mr-2"}>
                                        <LikeIcon/>
                                        <span className={"text-lg ml-1"}>2</span>
                                    </div>
                                    <div className={"flex items-center"}>
                                        <CommentIcon/>
                                        <span className={"text-lg ml-1"}>2</span>
                                    </div>

                                </div>
                                <div className={"cursor-pointer"}>
                                    <BookmarkIcon/>
                                </div>
                            </div>
                            <div className="flex w-full flex-wrap md:flex-nowrap mb-6 md:mb-0 gap-4">
                                <Input variant={"underlined"} placeholder="Write comment.." />
                            </div>
                        </div>
                    </CardFooter>
                    <Divider />
                </Card>
            ))}
        </>
    );
};

export default PostCard;
