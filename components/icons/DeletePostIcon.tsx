import React from 'react';

const DeletePostIcon = () => {
    return (
        <div>
            <svg width="14" height="16" viewBox="0 0 14 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path
                    d="M2.33333 5.33337V12C2.33333 13.4728 3.52724 14.6667 5 14.6667H9C10.4728 14.6667 11.6667 13.4728 11.6667 12V5.33337M8.33333 7.33337V11.3334M5.66667 7.33337L5.66667 11.3334M9.66667 3.33337L8.72916 1.92711C8.48187 1.55618 8.06556 1.33337 7.61975 1.33337H6.38025C5.93444 1.33337 5.51813 1.55618 5.27084 1.92711L4.33333 3.33337M9.66667 3.33337H4.33333M9.66667 3.33337H13M4.33333 3.33337H1"
                    stroke="#28303F" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
            </svg>

        </div>
    );
};

export default DeletePostIcon;