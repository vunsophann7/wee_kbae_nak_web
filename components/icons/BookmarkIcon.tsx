import React from 'react';

const BookmarkIcon = () => {
    return (
        <div>
            <svg width="16" height="20" viewBox="0 0 16 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path
                    d="M1.72238 1.58579C1.34731 1.96086 1.1366 2.46957 1.1366 3V19L8.1366 15.5L15.1366 19V3C15.1366 2.46957 14.9259 1.96086 14.5508 1.58579C14.1757 1.21071 13.667 1 13.1366 1H3.1366C2.60616 1 2.09746 1.21071 1.72238 1.58579Z"
                    stroke="#818187" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
            </svg>
        </div>
    );
};

export default BookmarkIcon;