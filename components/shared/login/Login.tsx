'use client'
import React from 'react';
import {Card, CardHeader, CardBody, CardFooter, Divider, Link, Image, Checkbox} from "@nextui-org/react";
import {Input} from "@nextui-org/input";
import {Button} from "@nextui-org/button";
import toast, {Toaster} from "react-hot-toast";
import {useRouter} from "next/navigation";
import {signIn} from "next-auth/react";
import { useState } from "react";


const Login = () => {

    const basePath = process.env.NEXT_PUBLIC_BASE_PATH ?? "";
    const defaultLogo = `${basePath}/asset/images/KbaeNak Logo KH.png`;
    const [userInfo, setUserInfo] = useState({ username: "", password: "" });
    const [loading, setLoading] = useState(false);
    const router = useRouter();

    const handleSubmit = async (e: React.FormEvent<HTMLFormElement>) => {
        // validate your userinfo
        e.preventDefault();

        if (userInfo.username === "" || userInfo.password === "") {
            toast.error("Username and Password is  required.");
            return;
        }

        const res = await signIn("credentials", {
            email: userInfo.username.replace(/\s/g, ""),
            password: userInfo.password,
            callbackUrl: "/",
            redirect: false,
        });

        if (res?.ok) {
            toast.success("Login Successfully.");
            router.push("/home");
        } else {
            toast.error("Username and Password is Incorrect.");
        }
    };

    return (
        <div className={"max-w-full max-h-full flex justify-center"}>
            <Card className="w-[400px]">
                <form
                    onSubmit={handleSubmit}
                >
                    <CardHeader className="flex flex-col gap-1 text-2xl">
                        Login
                    </CardHeader>
                    <Divider/>
                    <CardBody>

                        <Input
                            value={userInfo.username}
                            onChange={({target}) =>
                                setUserInfo({...userInfo, username: target.value})
                            }
                            type="text"
                            variant={"underlined"}
                            label="Email"
                            placeholder="Enter your username"/>
                        <Input
                            value={userInfo.password}
                            onChange={({target}) =>
                                setUserInfo({...userInfo, password: target.value})
                            }
                            type="password"
                            variant={"underlined"}
                            className={"mt-2"}
                            label="Password"
                            placeholder="Enter your password"/>
                        <div className="flex py-2 px-1 justify-between mt-2">
                            <Checkbox
                                classNames={{
                                    label: "text-small",
                                }}
                            >
                                Remember me
                            </Checkbox>
                            <Link color="primary" href="#" size="sm">
                                Forgot password?
                            </Link>
                        </div>

            </CardBody>
            <Divider/>
            <CardFooter className={"w-full flex justify-end"}>
                    <Button
                        type="submit"
                        color="primary"
                        variant="shadow"
                    >
                        Login
                    </Button>
                </CardFooter>
                </form>

            </Card>
            <Toaster />
        </div>
    );
};

export default Login;