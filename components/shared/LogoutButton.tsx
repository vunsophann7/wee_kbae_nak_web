"use client"

import React from 'react'
import {Button} from "@nextui-org/react";
import toast from "react-hot-toast";
import { signOut } from 'next-auth/react'

const LogoutButton = () => {
    return (
        <Button onClick={() => {
            signOut()
            toast.success("Logout Successfully.")
        }}>
            Logout
        </Button>
    )
}

export default LogoutButton