"use client"
import React from 'react'
import { useRouter } from 'next/navigation';
import {Button} from "@nextui-org/react";
import { signIn } from 'next-auth/react';

const ButtonLogin = () => {
  const router= useRouter();
  return (
    <div className='flex gap-2'>
    <Button
    onClick={() => {
      signIn();
    }}
  >
    Login
  </Button>
  <Button
      onClick={() => {
        router.push('signup');
      }}
    >
      Signup
    </Button>
    </div>
  )
}

export default ButtonLogin