import React, {useState} from 'react';
import {Modal, ModalContent, ModalHeader, ModalBody, ModalFooter, Button, useDisclosure} from "@nextui-org/react";
import {usePostStore} from "@/store";
import {User} from "@nextui-org/react";
import {Select, SelectItem} from "@nextui-org/react";
import {Input} from "@nextui-org/input";
import {Avatar} from "@nextui-org/react";
import useFetchProfile from "@/lib/hooks/use-fetch-profile";
import UploadImageIcon from "@/components/icons/UploadImageIcon";
import LocationIcon from "@/components/icons/LocationIcon";
export const SelectorIcon = (props:any) => (
    <svg
        aria-hidden="true"
        fill="none"
        focusable="false"
        height="1em"
        role="presentation"
        stroke="currentColor"
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeWidth="1.5"
        viewBox="0 0 24 24"
        width="1em"
        {...props}
    >
        <path d="M0 0h24v24H0z" fill="none" stroke="none" />
        <path d="M8 9l4 -4l4 4" />
        <path d="M16 15l-4 4l-4 -4" />
    </svg>
);
export const CameraIcon = ({fill, size, height, width, className, ...props}: {fill?: string, size?: number, height?: number, width?: number, className?: string, props?: any}) => {
    return (
        <svg
            className={className}
            fill="none"
            height={size || height || 24}
            viewBox="0 0 24 24"
            width={size || width || 24}
            xmlns="http://www.w3.org/2000/svg"
            {...props}
        >
            <path
                clipRule="evenodd"
                d="M17.44 6.236c.04.07.11.12.2.12 2.4 0 4.36 1.958 4.36 4.355v5.934A4.368 4.368 0 0117.64 21H6.36A4.361 4.361 0 012 16.645V10.71a4.361 4.361 0 014.36-4.355c.08 0 .16-.04.19-.12l.06-.12.106-.222a97.79 97.79 0 01.714-1.486C7.89 3.51 8.67 3.01 9.64 3h4.71c.97.01 1.76.51 2.22 1.408.157.315.397.822.629 1.31l.141.299.1.22zm-.73 3.836c0 .5.4.9.9.9s.91-.4.91-.9-.41-.909-.91-.909-.9.41-.9.91zm-6.44 1.548c.47-.47 1.08-.719 1.73-.719.65 0 1.26.25 1.72.71.46.459.71 1.068.71 1.717A2.438 2.438 0 0112 15.756c-.65 0-1.26-.25-1.72-.71a2.408 2.408 0 01-.71-1.717v-.01c-.01-.63.24-1.24.7-1.699zm4.5 4.485a3.91 3.91 0 01-2.77 1.15 3.921 3.921 0 01-3.93-3.926 3.865 3.865 0 011.14-2.767A3.921 3.921 0 0112 9.402c1.05 0 2.04.41 2.78 1.15.74.749 1.15 1.738 1.15 2.777a3.958 3.958 0 01-1.16 2.776z"
                fill='currentColor'
                fillRule="evenodd"
            />
        </svg>
    );
};
const PostPopup = () => {
    const {open, setOpen} = usePostStore(state => state);
    const animals = [
        { value: '1', label: 'Dog' },
        { value: '2', label: 'Cat' },
        { value: '3', label: 'Elephant' },
        { value: '4', label: 'Lion' },
    ];
    const variants = ["underlined"];

    const {profile} = useFetchProfile();
    console.log("profile", profile)
    return (
        <div>
            <Modal
                backdrop="opaque"
                isOpen={open}
                motionProps={{
                    variants: {
                        enter: {
                            y: 0,
                            opacity: 1,
                            transition: {
                                duration: 0.3,
                                ease: "easeOut",
                            },
                        },
                        exit: {
                            y: -20,
                            opacity: 0,
                            transition: {
                                duration: 0.2,
                                ease: "easeIn",
                            },
                        },
                    }
                }}
            >
                <ModalContent>
                    <ModalHeader className="w-full">
                        <User
                            name={profile?.data?.data?.full_nm}
                            description="Request"
                            avatarProps={{
                                src: profile?.data?.data?.usr_prof_img,
                            }}
                        />
                    </ModalHeader>
                    <ModalBody>
                        <div className="w-full flex flex-col gap-4">
                            {variants.map((variant) => (
                                <div key={variant} className="flex w-full flex-wrap md:flex-nowrap mb-6 md:mb-0 gap-4">
                                    <Input variant={"underlined"} placeholder="What's heppening?"/>
                                </div>
                            ))}
                        </div>
                        <div className={"w-full flex justify-between"}>
                            <Select
                                placeholder="Select an animal"
                                labelPlacement="outside"
                                className="w-8/12"
                                disableSelectorIconRotation
                                selectorIcon={<SelectorIcon/>
                                }
                            >
                                {animals.map((animal) => (
                                    <SelectItem key={animal.value} value={animal.value}>
                                        {animal.label}
                                    </SelectItem>
                                ))}
                            </Select>
                            <div className="flex gap-4 items-center">
                                <div className="flex items-center justify-center w-full">
                                    <label htmlFor="dropzone-file"
                                           className="flex flex-col items-center justify-center me-2 w-10 h-10 border-2 border-gray-300 border-dashed rounded-lg cursor-pointer bg-gray-50 dark:hover:bg-bray-800 dark:bg-gray-700 hover:bg-gray-100 dark:border-gray-600 dark:hover:border-gray-500 dark:hover:bg-gray-600">
                                        <div className="flex flex-col items-center justify-center">
                                            <UploadImageIcon/>
                                        </div>
                                        <input id="dropzone-file" type="file" className="hidden"/>
                                    </label>
                                    <label htmlFor="dropzone-file"
                                           className="flex flex-col items-center justify-center w-10 h-10 border-2 border-gray-300 border-dashed rounded-lg cursor-pointer bg-gray-50 dark:hover:bg-bray-800 dark:bg-gray-700 hover:bg-gray-100 dark:border-gray-600 dark:hover:border-gray-500 dark:hover:bg-gray-600">
                                        <div className="flex flex-col items-center justify-center">
                                            <LocationIcon/>
                                        </div>
                                        <input id="dropzone-file" type="file" className="hidden"/>
                                    </label>
                                </div>

                            </div>
                        </div>

                    </ModalBody>
                    <ModalFooter>
                        <Button color="danger" variant="light" onClick={() => setOpen(false)}>
                            Cancel
                        </Button>
                        <Button color="primary">
                            Create Post
                        </Button>
                    </ModalFooter>
                </ModalContent>
            </Modal>
        </div>
    );
};

export default PostPopup;