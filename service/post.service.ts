
import {http} from "@/utils/http";

const ServiceId = {
    POST: '/api/v1/post',
}

const getPostList = async (pageNumber: number, pageSize: number)  => {
    try {
        const result = await http.get(ServiceId.POST, {
            params: {
                page_number: pageNumber,
                page_size: pageSize
            }
        });
        return result?.data;
    } catch (err) {
        return err;
    }
}

export const postService = {
    getPostList
}