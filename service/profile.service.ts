import { http } from "@/utils/http";

const ServiceId = {
    PROFILE: '/api/v1/profile',
}

const getProfile = () => {
    return http.get(ServiceId.PROFILE);
}

export const profileService = {
    getProfile
}

export default profileService;