import { title } from "@/components/primitives";
import useFetchPostList from "@/lib/hooks/use-fetch-postList";

export default function DocsPage() {
	const {postList} = useFetchPostList();
	console.log("post data : ", postList?.data)
	return (
		<div>
			<h1 className={title()}>Profile</h1>
		</div>
	);
}
