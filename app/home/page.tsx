"use client"
import React from "react";
import useFetchProfile from "@/lib/hooks/use-fetch-profile";
import PostCard from "@/components/card/PostCard";
export default function HomePage() {

	// const {profile} = useFetchProfile();
	// console.log("profile", profile);

	return (
		<div>
			<div className="grid grid-cols-4 gap-3">
				<div>1</div>
				<div className={"col-span-2"}>
					<PostCard />
				</div>
				<div>3</div>
			</div>
		</div>
	);
}
