'use client'
import {useQuery} from "@tanstack/react-query";
import {postService} from "@/service/post.service";

const useFetchPostList = () => {

    const query = useQuery({
        queryKey: ["postList"],
        queryFn: async () => postService.getPostList(0, 10)
    })

    return {
        isLoading: query?.isLoading,
        isError: query?.isError,
        postList: query?.data ?? []
    }
}

export default useFetchPostList;