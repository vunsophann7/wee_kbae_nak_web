"use client"

import {useQuery} from "@tanstack/react-query";
import profileService from "@/service/profile.service";

const useFetchProfile = () => {

        const query = useQuery({
            queryKey: ["profile"],
            queryFn: async () => await profileService.getProfile()
        })

        return {
            isLoading: query?.isLoading,
            isError: query?.isError,
            profile: query?.data ?? []
        }
}

export default useFetchProfile;