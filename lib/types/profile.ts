export interface Profile {
    full_nm: string;
    role: string;
    usr_nm: string;
    usr_prof_img: string;
}