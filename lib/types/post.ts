export interface PostRequest {
    service_id: string;
    post_image: string;
    title: string;
}