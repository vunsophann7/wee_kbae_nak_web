export interface AuthRequest{
    usr_nm: string;
    usr_pwd: string;
}

export interface RegisterUserRequest {
    username: string,
}