export interface PostCardList {
    id: string;
    user: User;
    service: Service[];
    shopId: string;
    postImage: string;
    title: string;
    location: string;
    create_date: string;
    postStatus: string;
}

export interface User {
    id: string;
    username: string;
    fullName: string;
    userProfileImg: string;
}

export interface Service {
    id: string;
    name: string;
    category: Category;
}

export interface Category {
    id: string;
    name: string;
}
