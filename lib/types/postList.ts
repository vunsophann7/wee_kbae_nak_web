export interface PostList {
    // response in api

    // private Long id;
    // private Long userId;
    // private Long serviceId;
    // private Long shopId;
    // private String postImage;
    // private String title;
    // private String location;
    // private String createDate;
    // private String postStatus;

    id: string;
    userId: string;
    serviceId: string;
    shopId: string;
    postImage: string;
    title: string;
    location: string;
    createDate: string;
    postStatus: string;

}